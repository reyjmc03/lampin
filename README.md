# LAMPIN
Lamp Stack Server quick installation script

# FEATURES
- Add LAMP repositories
- Remove LAMP repositories
- Install LAMP(Linux, Apache, MySqL and PHP)


# REQUIREMENTS
- Python 2.7 or more
- An operating system (tested on Ubuntu 14.00 / 14.04LTS)
- later version of ubuntu must be developed soon.


# INSTALLATION
- sudo su
- git clone https://github.com/reyjmc03/lampin.git && cp lampin/lampin.py /usr/bin/lampin
- chmod +x /usr/bin/lampin
- sudo lampin


# USAGE
- Just select the number of a tool to install it
- Press 0 to install all tools
- back : Go back
- gohome : Go to the main menu


# CHANGE LOGS
**28 February 2016 15:34**
- updated all python script

**29 February 2016 10:23**
- create functions for apache, mysql, php, help, and about
- adding apache2 installation script
- adding mysql installation script
- adding php5 installation script

**29 February 2016 17:01**
- adding help content
- adding about content

**29 February 2016 10:28**
- fixing README.md file


# CONTACT
- Website : http://www.jomarrey.com
- Facebook : https://facebook.com/jomarrey03
- Email : reyjmc03@gmail.com


